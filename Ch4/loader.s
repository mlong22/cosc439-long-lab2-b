global loader                   

    MAGIC_NUMBER equ 0x1BADB002
    FLAGS        equ 0x0
    CHECKSUM     equ -MAGIC_NUMBER
    FB_GREEN     equ 8
    FB_DARK_GREY equ 2


    section .text
    align 4
        dd MAGIC_NUMBER
        dd FLAGS
        dd CHECKSUM
        dd FB_GREEN
        dd FB_DARK_GREY

    loader:
        mov eax, 0xCAFEBABE

        extern sum_of_three

	push dword 3
	push dword 2
	push dword 1
	call sum_of_three
	extern fb_write_cell
	call fb_write_cell
    	
    .loop:
        jmp .loop
        
    KERNEL_STACK_SIZE equ 4096
    
    section .bss
    align 4
    kernel_stack:
        resb KERNEL_STACK_SIZE
        
    mov esp, kernel_stack + KERNEL_STACK_SIZE
    mov [0x000B8000], word 0x2841



